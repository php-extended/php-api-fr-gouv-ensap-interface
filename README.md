# php-extended/php-api-fr-gouv-ensap-interface

A library that gets data from the ensap.gouv.fr website.

![coverage](https://gitlab.com/php-extended/php-api-fr-gouv-ensap-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-extended/php-api-fr-gouv-ensap-interface ^8`


## Basic Usage

This library is an interface-only library.

For a concrete implementation, see [`php-extended/php-api-fr-gouv-ensap-object`](https://gitlab.com/php-extended/php-api-fr-gouv-ensap-object).


## License

MIT (See [license file](LICENSE)).
