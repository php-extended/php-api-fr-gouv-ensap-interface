<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use InvalidArgumentException;
use Iterator;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Client\ClientExceptionInterface;
use RuntimeException;
use Stringable;

/**
 * ApiFrGouvEnsapEndpointInterface class file.
 * 
 * This class takes all entering requests and give back all the api responses,
 * decoded into their objet form.
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapEndpointInterface extends Stringable
{
	
	public const START_YEAR = 2016;
	public const CODE_SOUS_THEME_GED_BULLETIN = '43';
	public const CODE_SOUS_THEME_GED_ATTESTATION_FISCALE = '45';
	public const CODE_SOUS_THEME_GED_DECOMPTE_RAPPEL = '47';
	
	/**
	 * Logs in.
	 * 
	 * @param string $username
	 * @param string $password
	 * @return boolean
	 * @throws RuntimeException if the login fails
	 * @throws ClientExceptionInterface
	 * @throws UnprovidableThrowable
	 */
	public function login(string $username, string $password) : bool;
	
	/**
	 * Initializes the habilitations server side. MUST be called just after 
	 * the login.
	 * 
	 * @return ApiFrGouvEnsapHabilitationInterface
	 * @throws RuntimeException
	 * @throws ClientExceptionInterface
	 * @throws UnprovidableThrowable
	 */
	public function initialiserHabilitations() : ApiFrGouvEnsapHabilitationInterface;
	
	/**
	 * Gets the information about the logged in people.
	 * 
	 * @return ApiFrGouvEnsapAccueilConnecteInterface
	 * @throws RuntimeException
	 * @throws ClientExceptionInterface
	 * @throws UnprovidableThrowable
	 */
	public function getDataAccueilConnecte() : ApiFrGouvEnsapAccueilConnecteInterface;
	
	/**
	 * Gets the information about the connected profile.
	 * 
	 * @return ApiFrGouvEnsapProfileInterface
	 * @throws RuntimeException
	 * @throws ClientExceptionInterface
	 * @throws UnprovidableThrowable
	 */
	public function getDataProfil() : ApiFrGouvEnsapProfileInterface;
	
	/**
	 * Gets the information about the remuneration for the given year.
	 * 
	 * @param integer $year
	 * @return Iterator<integer, ApiFrGouvEnsapDocumentInterface>
	 * @throws RuntimeException
	 * @throws ClientExceptionInterface
	 * @throws UnprovidableThrowable
	 */
	public function getDataRemuneration(int $year) : Iterator;
	
	/**
	 * Gets the binary raw data of the document for given uuid.
	 * 
	 * @param UuidInterface $documentUuid
	 * @return ApiFrGouvEnsapRawDocumentInterface binary raw data
	 * @throws RuntimeException
	 * @throws ClientExceptionInterface
	 * @throws UnprovidableThrowable
	 */
	public function getRawDocument(UuidInterface $documentUuid) : ApiFrGouvEnsapRawDocumentInterface;
	
	/**
	 * Gets the bulletin parsed informations from the api document.
	 * 
	 * @param ApiFrGouvEnsapDocumentInterface $document
	 * @return ApiFrGouvEnsapBulletinInterface
	 * @throws RuntimeException
	 * @throws ClientExceptionInterface
	 * @throws UnprovidableThrowable
	 */
	public function getBulletinFromDocument(ApiFrGouvEnsapDocumentInterface $document) : ApiFrGouvEnsapBulletinInterface;
	
	/**
	 * Gets the bulletin from the raw document that was downloaded.
	 * 
	 * @param ApiFrGouvEnsapRawDocumentInterface $document
	 * @return ApiFrGouvEnsapBulletinInterface
	 * @throws RuntimeException
	 */
	public function getBulletinFromRawDocument(ApiFrGouvEnsapRawDocumentInterface $document) : ApiFrGouvEnsapBulletinInterface;
	
	/**
	 * Gets the bulletin data decoded from raw pdf data.
	 * 
	 * @param string $pdfBinaryString
	 * @return ApiFrGouvEnsapBulletinInterface
	 * @throws RuntimeException if the data cannot be parsed accordingly
	 */
	public function getBulletinFromBinaryString(string $pdfBinaryString) : ApiFrGouvEnsapBulletinInterface;
	
	/**
	 * Gets the bulletin data from an existing pdf file, without download.
	 * 
	 * @param string $pdfFilePath
	 * @return ApiFrGouvEnsapBulletinInterface
	 * @throws InvalidArgumentException if the file path does not point to a suitable bulletin pdf file
	 * @throws RuntimeException if the data cannot be parsed accordingly
	 */
	public function getBulletinFromFilePath(string $pdfFilePath) : ApiFrGouvEnsapBulletinInterface;
	
}
