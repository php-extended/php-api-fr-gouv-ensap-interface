<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Stringable;

/**
 * ApiFrGouvEnsapAccueilConnecteInterface interface file.
 * 
 * This represents actuality data that is given to the user after logging in.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapAccueilConnecteInterface extends Stringable
{
	
	/**
	 * Gets the message list used by the UI.
	 * 
	 * @return array<string, string>
	 */
	public function getMessage() : array;
	
	/**
	 * Gets the message list used as alerts.
	 * 
	 * @return array<string, string>
	 */
	public function getMessagealerte() : array;
	
	/**
	 * Gets the contents of the accueil data.
	 * 
	 * @return ApiFrGouvEnsapContenuInterface
	 */
	public function getContenu() : ApiFrGouvEnsapContenuInterface;
	
	/**
	 * Gets the public data of the user.
	 * 
	 * @return ApiFrGouvEnsapAccueilDonneeInterface
	 */
	public function getDonnee() : ApiFrGouvEnsapAccueilDonneeInterface;
	
	/**
	 * Gets all the parameters.
	 * 
	 * @return array<string, string>
	 */
	public function getParametrage() : array;
	
}
