<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Stringable;

/**
 * ApiFrGouvEnsapTextInterface interface file.
 * 
 * This represents html texts for paragraphs.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapTextInterface extends Stringable
{
	
	/**
	 * Gets the title of the text of the paragraph.
	 * 
	 * @return ?string
	 */
	public function getTitre() : ?string;
	
	/**
	 * Gets the full text of the paragraph.
	 * 
	 * @return string
	 */
	public function getTexte() : string;
	
}
