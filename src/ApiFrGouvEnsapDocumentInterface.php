<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;
use Stringable;

/**
 * ApiFrGouvEnsapDocumentInterface interface file.
 * 
 * This represents an document for the user.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapDocumentInterface extends Stringable
{
	
	/**
	 * Gets the uuid of the document relative to this event.
	 * 
	 * @return UuidInterface
	 */
	public function getDocumentUuid() : UuidInterface;
	
	/**
	 * Gets the code of the subtheme from the GED where those docs are stored.
	 * 
	 * @return ?string
	 */
	public function getCodeSousThemeGed() : ?string;
	
	/**
	 * Gets the first label of the document.
	 * 
	 * @return string
	 */
	public function getLibelle1() : string;
	
	/**
	 * Gets the second label of the document.
	 * 
	 * @return ?string
	 */
	public function getLibelle2() : ?string;
	
	/**
	 * Gets the third label of the document.
	 * 
	 * @return ?string
	 */
	public function getLibelle3() : ?string;
	
	/**
	 * Gets the date when this document was made available.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDocument() : DateTimeInterface;
	
	/**
	 * Gets the year of availability of the document.
	 * 
	 * @return int
	 */
	public function getAnnee() : int;
	
}
