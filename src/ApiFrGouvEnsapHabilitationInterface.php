<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Stringable;

/**
 * ApiFrGouvEnsapHabilitationInterface interface file.
 * 
 * This represents habilitations when logged into the ensap application.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapHabilitationInterface extends Stringable
{
	
	/**
	 * Gets the identity of the user.
	 * 
	 * @return ApiFrGouvEnsapIdentificationInterface
	 */
	public function getIdentification() : ApiFrGouvEnsapIdentificationInterface;
	
	/**
	 * Gets whether this habilitation is read only.
	 * 
	 * @return bool
	 */
	public function hasLectureSeule() : bool;
	
	/**
	 * Gets the annees where the remuneration is available.
	 * 
	 * @return array<int, int>
	 */
	public function getListeAnneeRemuneration() : array;
	
	/**
	 * Gets the list of services this habilitation gives access.
	 * 
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function getListeService() : ApiFrGouvEnsapListeServiceInterface;
	
}
