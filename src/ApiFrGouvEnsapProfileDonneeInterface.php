<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use PhpExtended\Email\EmailAddressInterface;
use Stringable;

/**
 * ApiFrGouvEnsapProfileDonneeInterface interface file.
 * 
 * This represents the specific data about the connected user.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapProfileDonneeInterface extends Stringable
{
	
	/**
	 * Gets whether the current user has subscribed to notifications.
	 * 
	 * @return bool
	 */
	public function hasEstAbonneNotification() : bool;
	
	/**
	 * Gets whether the current user has subscribed to ensap specific
	 * informations.
	 * 
	 * @return ?bool
	 */
	public function hasEstAbonneMailEnsap() : ?bool;
	
	/**
	 * Gets the email address used for notifications.
	 * 
	 * @return ?EmailAddressInterface
	 */
	public function getMailNotification() : ?EmailAddressInterface;
	
	/**
	 * Gets the email address used as principal.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getMailPrincipal() : EmailAddressInterface;
	
	/**
	 * Gets the email address used as secondary.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getMailSecondaire() : EmailAddressInterface;
	
	/**
	 * Gets a string representing the source of the user.
	 * 
	 * @return string
	 */
	public function getModeInscriptionUsager() : string;
	
	/**
	 * Gets the no.secu of the user.
	 * 
	 * @return string
	 */
	public function getNir() : string;
	
	/**
	 * Gets the last name of the user.
	 * 
	 * @return string
	 */
	public function getNom() : string;
	
	/**
	 * Gets the first name of the user.
	 * 
	 * @return string
	 */
	public function getPrenom() : string;
	
	/**
	 * Gets the sex code of the user.
	 * 
	 * @return string
	 */
	public function getCodeSexe() : string;
	
}
