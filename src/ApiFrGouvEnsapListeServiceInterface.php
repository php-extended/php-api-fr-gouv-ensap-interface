<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Stringable;

/**
 * ApiFrGouvEnsapListeServiceInterface interface file.
 * 
 * This lists all the services and specifies which ones are available.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapListeServiceInterface extends Stringable
{
	
	/**
	 * Gets whether the service compte individuel retraite is available.
	 * 
	 * @return bool
	 */
	public function hasCompteindividuelretraite() : bool;
	
	/**
	 * Gets whether the service declaration maladie professionnelle? is
	 * available.
	 * 
	 * @return bool
	 */
	public function hasDeclamep() : bool;
	
	/**
	 * Gets whether the service demande depart retraite is available.
	 * 
	 * @return bool
	 */
	public function hasDemandedepartretraite() : bool;
	
	/**
	 * Gets whether the service demande départ retraite progressive is
	 * available.
	 * 
	 * @return bool
	 */
	public function hasDemandedepartretraiteprogressive() : bool;
	
	/**
	 * Gets whether the service document employeur is available.
	 * 
	 * @return bool
	 */
	public function hasDocumentemployeur() : bool;
	
	/**
	 * Gets whether the service mesap is available.
	 * 
	 * @return bool
	 */
	public function hasMesap() : bool;
	
	/**
	 * Gets whether the service pension is available.
	 * 
	 * @return bool
	 */
	public function hasPension() : bool;
	
	/**
	 * Gets whether the service remuneration is available.
	 * 
	 * @return bool
	 */
	public function hasRemuneration() : bool;
	
	/**
	 * Gets whether the service remuneration pension is available.
	 * 
	 * @return bool
	 */
	public function hasRemunerationpension() : bool;
	
	/**
	 * Gets whether the service retraite is available.
	 * 
	 * @return bool
	 */
	public function hasRetraite() : bool;
	
	/**
	 * Gets whether the service simulation is available.
	 * 
	 * @return bool
	 */
	public function hasSimulation() : bool;
	
	/**
	 * Gets whether the service suivi depart retraite is available.
	 * 
	 * @return bool
	 */
	public function hasSuividepartretraite() : bool;
	
	/**
	 * Gets whether the service titre pension is available.
	 * 
	 * @return bool
	 */
	public function hasTitrepension() : bool;
	
	/**
	 * Gets whether the service suivi départ retraite progressive is
	 * available.
	 * 
	 * @return bool
	 */
	public function hasSuividepartretraiteprogressive() : bool;
	
}
