<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Stringable;

/**
 * ApiFrGouvEnsapLoginResponseInterface interface file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapLoginResponseInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapLoginResponseInterface extends Stringable
{
	
	/**
	 * Gets the error code of the login.
	 * 
	 * @return string
	 */
	public function getError() : string;
	
	/**
	 * Gets the hash if of the connection.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the code of the response.
	 * 
	 * @return int
	 */
	public function getCode() : int;
	
	/**
	 * Gets the message of the response.
	 * 
	 * @return string
	 */
	public function getMessage() : string;
	
	/**
	 * Gets the result of the response.
	 * 
	 * @return int
	 */
	public function getResult() : int;
	
}
