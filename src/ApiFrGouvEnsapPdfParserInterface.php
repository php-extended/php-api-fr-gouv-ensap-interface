<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use InvalidArgumentException;
use RuntimeException;
use Stringable;

/**
 * ApiFrGouvEnsapPdfParserInterface class file.
 * 
 * The pdf parser that transforms raw pdf data into php structures.
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapPdfParserInterface extends Stringable
{
	
	/**
	 * Gets the bulletin information parsed from pdf binary string.
	 * 
	 * @param string $pdfBinaryString
	 * @return ApiFrGouvEnsapBulletinInterface
	 * @throws RuntimeException
	 */
	public function parseBulletinFromPdfString(string $pdfBinaryString) : ApiFrGouvEnsapBulletinInterface;
	
	/**
	 * Gets the bulletin information parsed from pdf file at path.
	 * 
	 * @param string $filePath
	 * @return ApiFrGouvEnsapBulletinInterface
	 * @throws RuntimeException
	 * @throws InvalidArgumentException
	 */
	public function parseBulletinFromFile(string $filePath) : ApiFrGouvEnsapBulletinInterface;
	
}
