<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Stringable;

/**
 * ApiFrGouvEnsapRawDocumentInterface interface file.
 * 
 * This represents the raw document from the api.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapRawDocumentInterface extends Stringable
{
	
	/**
	 * Gets the status code of the http request.
	 * 
	 * @return int
	 */
	public function getStatusCode() : int;
	
	/**
	 * Gets the file name of the document.
	 * 
	 * @return string
	 */
	public function getFileName() : string;
	
	/**
	 * Gets the length of the document.
	 * 
	 * @return int
	 */
	public function getLength() : int;
	
	/**
	 * Gets the mime type of the document.
	 * 
	 * @return string
	 */
	public function getMimeType() : string;
	
	/**
	 * Gets the raw data of the document.
	 * 
	 * @return string
	 */
	public function getRawData() : string;
	
}
