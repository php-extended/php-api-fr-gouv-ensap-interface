<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Stringable;

/**
 * ApiFrGouvEnsapProfileInterface interface file.
 * 
 * This represents profile data that is given by the app to the current user.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapProfileInterface extends Stringable
{
	
	/**
	 * Gets the contenu of this help texts for this profile.
	 * 
	 * @return ApiFrGouvEnsapContenuInterface
	 */
	public function getContenu() : ApiFrGouvEnsapContenuInterface;
	
	/**
	 * Gets the data about the logged user.
	 * 
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function getDonnee() : ApiFrGouvEnsapProfileDonneeInterface;
	
	/**
	 * Gets all the translations for the available messages.
	 * 
	 * @return array<string, string>
	 */
	public function getMessage() : array;
	
	/**
	 * Gets all the translations for alert messages.
	 * 
	 * @return array<string, string>
	 */
	public function getMessagealerte() : array;
	
	/**
	 * Gets all the parameters.
	 * 
	 * @return array<string, string>
	 */
	public function getParametrage() : array;
	
}
