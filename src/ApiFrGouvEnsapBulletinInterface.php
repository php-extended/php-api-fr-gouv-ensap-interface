<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use DateTimeInterface;
use Stringable;

/**
 * ApiFrGouvEnsapBulletinInterface interface file.
 * 
 * This reprsents the values that are embedded into a single bulletin.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapBulletinInterface extends Stringable
{
	
	/**
	 * Gets the emitter of the bulletin.
	 * 
	 * @return ?string
	 */
	public function getEmitter() : ?string;
	
	/**
	 * Gets the month of emission, month in letters, year in digits.
	 * 
	 * @return ?string
	 */
	public function getMonthLetter() : ?string;
	
	/**
	 * Gets the numero of the order to be paid.
	 * 
	 * @return ?string
	 */
	public function getNumOrder() : ?string;
	
	/**
	 * Gets the quantity of worked time.
	 * 
	 * @return ?string
	 */
	public function getWorkTime() : ?string;
	
	/**
	 * Gets the number of the affectation place (first line).
	 * 
	 * @return ?string
	 */
	public function getPoste1() : ?string;
	
	/**
	 * Gets the number of the affectation place (second line).
	 * 
	 * @return ?string
	 */
	public function getPoste2() : ?string;
	
	/**
	 * Gets the label of the affectation place.
	 * 
	 * @return ?string
	 */
	public function getLibellePoste() : ?string;
	
	/**
	 * Gets the siret number of the employer.
	 * 
	 * @return ?string
	 */
	public function getSiretEmployeur() : ?string;
	
	/**
	 * Gets the ministry number of the agent.
	 * 
	 * @return ?int
	 */
	public function getIdMin() : ?int;
	
	/**
	 * Gets the no secu of the agent.
	 * 
	 * @return ?string
	 */
	public function getIdNir() : ?string;
	
	/**
	 * Gets the no dossier of the agent.
	 * 
	 * @return ?int
	 */
	public function getIdNodos() : ?int;
	
	/**
	 * Gets the grade of the agent.
	 * 
	 * @return ?string
	 */
	public function getGrade() : ?string;
	
	/**
	 * Gets the number of children the agent has at charge.
	 * 
	 * @return ?int
	 */
	public function getChildCharged() : ?int;
	
	/**
	 * Gets the echelon of the agent.
	 * 
	 * @return ?string
	 */
	public function getEchelon() : ?string;
	
	/**
	 * Gets the index of the agent (or effective number of hours worked).
	 * 
	 * @return ?int
	 */
	public function getIndiceOrNbHours() : ?int;
	
	/**
	 * Gets the hour rate of payment.
	 * 
	 * @return ?int
	 */
	public function getTauxHoraire() : ?int;
	
	/**
	 * Gets whether the agent is at part time.
	 * 
	 * @return ?string
	 */
	public function getTpsPartiel() : ?string;
	
	/**
	 * Gets the lines of payment on the bulletin.
	 * 
	 * @return array<int, ApiFrGouvEnsapBulletinLineInterface>
	 */
	public function getLines() : array;
	
	/**
	 * Gets the total amount to be paid (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getTotalAPayer() : ?int;
	
	/**
	 * Gets the total amount to be deduced (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getTotalADeduire() : ?int;
	
	/**
	 * Gets the total taxes to be paid by the employer (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getTotalPourInfo() : ?int;
	
	/**
	 * Gets the total amount to be paid by the employer (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getTotalEmployeur() : ?int;
	
	/**
	 * Gets the total net amount to receive (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getTotalNetAPayer() : ?int;
	
	/**
	 * Gets the base ss for the year (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getBaseSsYear() : ?int;
	
	/**
	 * Gets the base ss for the month (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getBaseSsMonth() : ?int;
	
	/**
	 * Gets the montant imposable for the year (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getMontantImposableYear() : ?int;
	
	/**
	 * Gets the montant imposable for the month (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getMontahtImposableMonth() : ?int;
	
	/**
	 * Gets the assignated comptable office.
	 * 
	 * @return ?string
	 */
	public function getComptable() : ?string;
	
	/**
	 * Gets when this bulletin was paid.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDatePaid() : ?DateTimeInterface;
	
	/**
	 * Gets the iban of the account this bulletin was paid to.
	 * 
	 * @return ?string
	 */
	public function getAccountPaidIban() : ?string;
	
	/**
	 * Gets the bic of the account this bulletin was paid to.
	 * 
	 * @return ?string
	 */
	public function getAccountPaidBic() : ?string;
	
}
