<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Stringable;

/**
 * ApiFrGouvEnsapBulletinLineInterface interface file.
 * 
 * This represents a line on the bulletin.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapBulletinLineInterface extends Stringable
{
	
	/**
	 * Gets gets the code of the line.
	 * 
	 * @return ?int
	 */
	public function getCode() : ?int;
	
	/**
	 * Gets gets the libelle of the line.
	 * 
	 * @return string
	 */
	public function getLibelle() : string;
	
	/**
	 * Gets gets the amount to be paid for this line (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getAPayer() : ?int;
	
	/**
	 * Gets gets the amount to be deduced from this line (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getADeduire() : ?int;
	
	/**
	 * Gets gets the amount the boss paid for this line (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getPourInfo() : ?int;
	
}
