<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Stringable;

/**
 * ApiFrGouvEnsapAccueilDonneeInterface interface file.
 * 
 * This represents data about the user.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapAccueilDonneeInterface extends Stringable
{
	
	/**
	 * Gets the information of identification of the user.
	 * 
	 * @return ApiFrGouvEnsapIdentificationInterface
	 */
	public function getIdentification() : ApiFrGouvEnsapIdentificationInterface;
	
	/**
	 * Gets the list of events for the user.
	 * 
	 * @return array<int, ApiFrGouvEnsapEvenementInterface>
	 */
	public function getListeEvenement() : array;
	
	/**
	 * Gets the list of years the event are dispatched on.
	 * 
	 * @return array<int, int>
	 */
	public function getListeAnneeRemuneration() : array;
	
}
