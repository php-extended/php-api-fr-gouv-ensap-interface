<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Stringable;

/**
 * ApiFrGouvEnsapContenuInterface interface file.
 * 
 * This represents contents about the UI.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapContenuInterface extends Stringable
{
	
	/**
	 * Gets the messages of the contenu, indexed by field name.
	 * 
	 * @return array<string, string>
	 */
	public function getChamp() : array;
	
	/**
	 * Gets the paragraphes of the contenu, indexed by field name.
	 * 
	 * @return array<string, ApiFrGouvEnsapTextInterface>
	 */
	public function getParagraphe() : array;
	
}
