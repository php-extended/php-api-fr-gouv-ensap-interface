<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;
use Stringable;

/**
 * ApiFrGouvEnsapEvenementInterface interface file.
 * 
 * This represents an event for the user.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74InterfaceMetadata
 * 
 * @author Anastaszor
 */
interface ApiFrGouvEnsapEvenementInterface extends Stringable
{
	
	/**
	 * Gets the id of the event.
	 * 
	 * @return int
	 */
	public function getEvenementId() : int;
	
	/**
	 * Gets the date of the event.
	 * 
	 * @return string
	 */
	public function getDateEvenement() : string;
	
	/**
	 * Gets the uuid of the document relative to this event.
	 * 
	 * @return UuidInterface
	 */
	public function getDocumentUuid() : UuidInterface;
	
	/**
	 * Gets the first label of the event.
	 * 
	 * @return string
	 */
	public function getLibelle1() : string;
	
	/**
	 * Gets the second label of the event.
	 * 
	 * @return ?string
	 */
	public function getLibelle2() : ?string;
	
	/**
	 * Gets the third label of the event.
	 * 
	 * @return ?string
	 */
	public function getLibelle3() : ?string;
	
	/**
	 * Gets the statut of the document. May be 'EVE_LU', meaning the document
	 * has been read, or 'EVE_NON_LU', meaning the document has not been read.
	 * 
	 * @return string
	 */
	public function getStatut() : string;
	
	/**
	 * Gets the date when this document was read.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateLecture() : ?DateTimeInterface;
	
	/**
	 * Gets the label for the icon of the event.
	 * 
	 * @return ?string
	 */
	public function getLibelleIcone() : ?string;
	
	/**
	 * Gets the id of the icon for the event.
	 * 
	 * @return ?string
	 */
	public function getIcone() : ?string;
	
	/**
	 * Gets the sort number ordering.
	 * 
	 * @return ?int
	 */
	public function getTri() : ?int;
	
	/**
	 * Gets the action that should be launched wien clicked on this event.
	 * 
	 * @return string
	 */
	public function getActionIhm() : string;
	
	/**
	 * Gets the service of the event.
	 * 
	 * @return string
	 */
	public function getService() : string;
	
}
